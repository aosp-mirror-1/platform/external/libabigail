# This libabigail repository is unmaintained

Android no longer uses libabigail and has instead moved to STG. As a result,
this repository is unmaintained and has only historical value.

If you are looking for an ABI monitoring solution, we can recommend STG.

* [STG](https://android.googlesource.com/platform/external/stg/): The
  Symbol-Type Graph project
     * This was created by and is maintained by Android developers.
     * It contains fast, reliable tools for ABI extraction and comparison.
     * Its native file format is version control friendly.

If you are looking for libabigail, please refer to the upstream project.

* [libabigail](https://sourceware.org/libabigail/): The ABI Generic Analysis and
  Instrumentation Library
     * This was last merged here at commit `e8a012b6e19b` on 2022-06-22.
